<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => '<div class="header-quote">"Felices para siempre no es un cuento de hadas . Es una elección".</div>',
                'brandUrl' => '#',
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
            ];

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <section id="intro" data-speed="6" data-type="background">
                <div class="textIntro">
                    <div class="header animated tada">
                        <h1>Ady &amp; Jenny</h1>
                        <h4>NUESTRO MATRIMONIO CIVIL</h4>
                        <hr>
                        <h4>Sabado, 7 de Mayo 2016</h4>
                    </div>
                    <!---font size="16" color="white">Prueba</font--->
                </div>
            </section>

            <!-- Section #2 -->
            <section id="home" style="background-position: 50% 0px;" data-speed="4" data-type="background">
                <div class="container">
                    <h3>El &amp; Ella. Su Historia Feliz!</h3>
                    <div class="heart content-style"></div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="shape">
                                <a href="#" class="overlay hexagon"></a>
                                <div class="base">
                                    <img src="images/novia.png"  height="245px" width="245px" alt="">
                                </div>
                            </div>
                            <h4 class="bride-name">Jenny Oliveira</h4>
                            <h3 class="bride-description">Novia</h3>
                            <div class="social">
                                <a href="#"><i class="fa-facebook"></i></a>
                                <a href="#"><i class="fa-twitter"></i></a>
                            </div>
                        </div><!--end col-md-3-->
                        <div class="col-md-6">
                            <p>.</p>

                            <p>.</p>

                            <p>.</p>

                            <p>. . . </p>
                        </div><!--end-col-md-6-->
                        <div class="col-md-3">
                            <div class="shape">
                                <a href="#" class="overlay hexagon"></a>
                                <div class="base">
                                    <img src="images/novio.png"  height="245px" width="245px" alt="">
                                </div>
                            </div>
                            <h4 class="groom-name">Ady Prado</h4>
                            <h3 class="groom-description">Novio</h3>
                            <div class="social">
                                <a href="#"><i class="fa-facebook"></i></a>
                                <a href="#"><i class="fa-twitter"></i></a>
                            </div>
                        </div><!--end col-md-3-->
                    </div><!--end row-->


                </div>
            </section>

            <!-- Section #3 -->
            <section id="about" data-speed="2" data-type="background">
                <div class="container">
                    <div class="row-fluid">
                        <div class="container" style="align-content: center; margin-right: -254px">
                            <div class="your-clock"></div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="parents" data-speed="2" data-type="background">
                <div class="container">

                    <!--Parents-->
                    <div id="parents" class="animated fadeIn">
                        <h3>Los Padres de la Feliz Pareja</h3>
                        <div class="heart content-style"></div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="parents">
                                    <a href="#" class="overlay hexagon"></a>
                                    <div class="base">
                                        <img src="images/blanca_jaramillo" height="190px" width="190px" alt="">
                                    </div>
                                    <div class="parents-text-women"><h4>Blanca Jaramillo</h4></div>
                                </div>
                            </div><!--end col-md-2-->
                            <div class="col-md-2">
                                <div class="parents">
                                    <a href="#" class="overlay hexagon"></a>
                                    <div class="base">
                                        <img src="images/antonio_oliveira.png" height="190px" width="190px" alt="">
                                    </div>
                                    <div class="parents-text-men"><h4>Antonio De Oliveira</h4></div>
                                </div>
                            </div><!--end col-md-2-->
                            <div class="col-md-4">
                                <div class="parents-text"><h2>« &nbsp; De Ella &nbsp; &amp;  &nbsp; De El &nbsp; » </h2></div>
                            </div><!--end col-md-4-->
                            <div class="col-md-2">
                                <div class="parents">
                                    <a href="#" class="overlay hexagon"></a>
                                    <div class="base">
                                        <img src="images/consuelo_salas.png" height="190px" width="190px" alt="">
                                    </div>
                                    <div class="parents-text-women"><h4>Consuelo Salas</h4></div>
                                </div>
                            </div><!--end col-md-2-->
                            <div class="col-md-2">
                                <div class="parents">
                                    <a href="#" class="overlay hexagon"></a>
                                    <div class="base">
                                        <img src="images/jose_prado.png" height="190px" width="190px" alt="">
                                    </div>
                                    <div class="parents-text-men"><h4>Jose Prado</h4></div>
                                </div>
                            </div><!--end col-md-2-->
                        </div><!-- end row -->
                    </div><!--end #parents-->


                    <!--Godfathers-->
                    <div id="godfathers" class="animated fadeIn">
                        <h3>Los Padrinos</h3>
                        <div class="heart content-style"></div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="godfathers-text-women"><h4>Valeria Acevedo</h4>
                                    <p>.</p></div>
                            </div><!--end col-md-4-->
                            <div class="col-md-2">
                                <div class="godfathers">
                                    <div class="overlay hexagon"></div>
                                    <div class="godfathers-img"><img src="images/valeria_acevedo.png" width='185px' height='185px' alt=""></div>
                                </div>
                            </div><!--end col-md-2-->
                            <div class="col-md-2">
                                <div class="godfathers">
                                    <div class="overlay hexagon"></div>
                                    <div class="godfathers-img"><img src="images/juan_velasco.png" width='185px' height='185px' alt=""></div>
                                </div>
                            </div><!--end col-md-2-->
                            <div class="col-md-4">
                                <div class="godfathers-text-men"><h4>Juan Velasco</h4>
                                    <p>.</p></div>
                            </div><!--end col-md-4-->
                        </div><!--end row-->
                    </div><!--end #godfathers-->
                </div>
            </section>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
