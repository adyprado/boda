var clock = $('.your-clock').FlipClock({
    countdown : true,
    clockFace: 'DailyCounter',
    language: 'es',
});

var currentDate = new Date();
var futureDate  = new Date("2016/05/07 15:00:00");
var diff = (futureDate.getTime() - currentDate.getTime())/1000;

clock.setCountdown(true);
clock.setTime(diff);
clock.start();